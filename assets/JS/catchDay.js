

    export default (date, year) => {
      let [month, day] = date.split("/");
      let monthAndDay = new Date(`${month}/${day}/${year}`);
      let thatDay = monthAndDay.getDay();

      if(thatDay === 0) {
        return 6;
      }
      
      console.log(monthAndDay)
      return monthAndDay.getDay() - 1;
    } 
  



  