import catching from "./catchDay.js"
import styling from "./dynamicStyling.js";

export default class search{ 
    constructor() {
        this.jsonData = ".json-data";
        this.years = ".year";
        this.year = 0;
        this.json = {};
        this.btn = ".submit";
        console.log(this.years);
      }


    complete = () => {
        this.makeEvent()
    }

    makeEvent = () => {
        let elem = document.querySelector(this.btn);
        elem.addEventListener("click", this.makeClick);
    }
    
    // making the click event
    makeClick = () => {
    let json = this.takeJsonData();
    let year = this.takeYear();

        if(json && year) {
            this.workingJson();
        }
    }

      // Taking json from the textarea
      takeJsonData = () => {
        let elem = document.querySelector(this.jsonData.innerHTML);
          this.jsonData = JSON.parse(elem);
          return true;
          return false;
      }
    
      // Taking year from input 
      takeYear = () => {
        let elem = document.querySelector(this.year);
        this.year = elem.value;
    
        if(this.year) {
          alert("Invalid Year");
          return false;
        }
    
        return true;
      }
    
      // taking name and date from json.
      workingJson = () => {
        let json = this.jsonData;
          json.forEach( elem => {
            let {name, birthday} = elem;
            let initials = this.takeInitial(name);
            let dateDay = catching(birthday, this.year);
    
            if(dateDay !== false) {
              this.daysData.push({dateDay, initials});
            }
          });

      }
    
      // taking the initials of the name
      takeInitial = name => {
        let arr = name.split(" ");
        if(arr.length < 2)
        return name[0].toUpperCase() + name[1].toUpperCase(); 
        return arr[0][0].toUpperCase() + arr[1][0].toUpperCase();
      }
    
}